grammar Regex;

re
    : alt EOF
    ;

alt
:
exprs+= expression('|' exprs+=expression)*
;

expression
: 
    (elems+=element)*
;

element
: a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    | '(' e=alt ')' #Parenthesis
    | '(' e=alt ')' m=MODIFIER #ModifierParenthesis
;

atom
    : c=CHARACTER
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
